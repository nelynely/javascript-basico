// Normal function
function lowerName(name) {
  return name.toLowerCase();
}
console.log(lowerName("nely"));

// Other  normal function
function upperName(name) {
  return name.toUpperCase;
}
console.log("nely");

// Arrow function
const upperName = name => {
  return name.toUpperCase();
};
console.log(upperName("nelynely"));

const upperName = name => name.toUpperCase();
console.log("my name");

const countLetter = word => word.length;
console.log(countLetter("harry"));
