const parOuImpar = n => {
  return n % 2 === 0;
};
console.log(parOuImpar(3));
const soma = (n1, n2) => {
  return n1 + n2;
};
console.log(soma(2, 2));

const potencia = valor => {
  return valor ** 2;
};

const areaQuadrado = (base, altura) => {
  return base * altura;
};

let consoleTeste = () => console.log("Fala aí");
consoleTeste();

let imc = (peso, altura) => {
  return peso / altura ** 2;
};
console.log(imc(80, 1.8));

const calcIdade = (anoAtual, anoNasc) => {
  return anoAtual - anoNasc;
};
console.log(calcIdade(2020, 1987));
