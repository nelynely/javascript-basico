setTimeout(() => {
  console.log("Arrow Functions");
}, 2000);

function myFunc(otherFunc) {
  console.log(otherFunc());
}

myFunc(() => 123);
