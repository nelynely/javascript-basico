var c = "a";
if (c === "a" || c === "e" || c === "i" || c === "o" || c === "u") {
  console.log("A");
} else if (c === "b" || c === "c" || c === "d" || c === "f" || c === "g") {
  console.log("B");
} else if (c === "h" || c === "j" || c === "k" || c === "l" || c === "m") {
  console.log("C");
} else if (
  c === "n" ||
  c === "p" ||
  c === "q" ||
  c === "r" ||
  c === "s" ||
  c === "t" ||
  c === "v" ||
  c === "w" ||
  c === "y" ||
  c === "w"
) {
  console.log("D");
}
